#include <SDL.h>

#include <glbinding/gl/gl.h>        // all of the headers below, meaning the complete OpenGL API
#include <glbinding/Binding.h>
using namespace gl;
using namespace glbinding;

#include <glm/glm.hpp>
using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat3x3;
using glm::mat4x4;
using glm::quat;

#include <iostream>

class TestViewGl
{
    const vec4 mClearColor = vec4(0.5);
    const char *vertexShaderSource = "#version 320 es\n"
        "precision highp float;\n"
        "layout (location = 0) in vec3 aPos;\n"
        "void main()\n"
        "{\n"
        "   gl_Position = vec4(aPos.x, aPos.y, aPos.z, 1.0);\n"
        "}\0";
    const char *fragmentShaderSource = "#version 320 es\n"
        "precision highp float;\n"
        "out vec4 FragColor;\n"
        "void main()\n"
        "{\n"
        "   FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
        "}\n\0";

public:
    TestViewGl();
    ~TestViewGl();

    void initGl();
    void renderFrame();
    void resize(unsigned int width, unsigned int height);

private:
    // example data
    unsigned int VBO, VAO, EBO;
    unsigned int shaderProgram;

    void buildExample();

};