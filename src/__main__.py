
from PyQt5.QtWidgets import *
from pythonContextWidget import PythonContextWidget

app = QApplication([])
contextWidget = PythonContextWidget()
guiWidget = QWidget()
guiLayout = QHBoxLayout()
guiWidget.setLayout(guiLayout)
guiLayout.addWidget(contextWidget)
guiWidget.setFixedSize(800, 600)
guiWidget.show()

app.exec_()