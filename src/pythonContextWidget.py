
from PyQt5 import QtCore, QtGui, QtOpenGL
from PyQt5.QtOpenGL import QGLWidget
import testView

class PythonContextWidget(QGLWidget):
    def __init__(self):
        super().__init__()
        self.inited = False

    def initializeGL(self):
        assert self.inited == False
        glFormat = QtOpenGL.QGLFormat()
        glFormat.setVersion(3, 2)
        glFormat.setProfile(QtOpenGL.QGLFormat.CoreProfile)
        testView.init()
        self.inited = True
        self.resizeGL(800, 600)

    def paintGL(self):
        if self.inited == False:
            return False
        testView.render()


    def resizeGL(self, w, h):
        if self.inited == False:
            return False
        testView.resize() # now 800x600 only