#include "testViewGL.h"
#define PY_SSIZE_T_CLEAN
#include <Python.h>

auto testViewGl = TestViewGl();

PyObject* py_initContext(PyObject* self, PyObject* args)
{
	testViewGl.initGl();
	Py_RETURN_NONE;
}

PyObject* py_render(PyObject* self, PyObject* args)
{
	testViewGl.renderFrame();
	Py_RETURN_NONE;
}

PyObject* py_resize(PyObject* self, PyObject* args)
{
	testViewGl.resize(800, 600);
	Py_RETURN_NONE;
}

static PyMethodDef testViewMethods[] =
{
	{ "init",   (PyCFunction)py_initContext, METH_VARARGS, "init gl" },
	{ "render", (PyCFunction)py_render,      METH_VARARGS, "render frame" },
	{ "resize", (PyCFunction)py_resize,      METH_VARARGS, "resizing" },
	{ NULL, NULL, 0, NULL }
};

static struct PyModuleDef testViewModule =
{
	PyModuleDef_HEAD_INIT,
	"SoilVisualize", /* name of module */
	"usage: Test pyqt c++ render\n",
	-1,   /* size of per-interpreter state of the module, or -1 if the module keeps state in global variables. */
	testViewMethods
};

PyMODINIT_FUNC PyInit_testView(void)
{
	return PyModule_Create(&testViewModule);
}
